package com.taledir.microservices;

import java.util.Date;
import java.util.Set;

public class WritingDto {

	private String title;

  private Delta body;
  
  private String owner;

  private String lastPublishedTitle;

  private String subtitle;

  private String snippet;

  private Date lastPublishedDate;

  private Date lastModifiedDate;

  private Boolean isEditingLocked;

  Set<String> authors;

  String publishedRoute;

  protected WritingDto() {

  }

  public WritingDto(String title, Delta body, String owner, String lastPublishedTitle, 
    String subtitle, String snippet, Date lastPublishedDate, Date lastModifiedDate, 
    Set<String> authors, String publishedRoute, Boolean isEditingLocked) {
    this.title = title;
    this.body = body;
    this.owner = owner;
    this.lastPublishedTitle = lastPublishedTitle;
    this.subtitle = subtitle;
    this.snippet = snippet;
    this.lastPublishedDate = lastPublishedDate;
    this.lastModifiedDate = lastModifiedDate;
    this.authors = authors;
    this.publishedRoute = publishedRoute;
    this.isEditingLocked = isEditingLocked;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Delta getBody() {
    return body;
  }

  public void setBody(Delta body) {
    this.body = body;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public String getLastPublishedTitle() {
    return lastPublishedTitle;
  }

  public void setLastPublishedTitle(String lastPublishedTitle) {
    this.lastPublishedTitle = lastPublishedTitle;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getSnippet() {
    return snippet;
  }

  public void setSnippet(String snippet) {
    this.snippet = snippet;
  }

  public Date getLastPublishedDate() {
    return lastPublishedDate;
  }

  public void setLastPublishedDate(Date lastPublishedDate) {
    this.lastPublishedDate = lastPublishedDate;
  }

  public Date getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(Date lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }

  public Set<String> getAuthors() {
    return authors;
  }

  public void setAuthors(Set<String> authors) {
    this.authors = authors;
  }

  public String getPublishedRoute() {
    return publishedRoute;
  }

  public void setPublishedRoute(String publishedRoute) {
    this.publishedRoute = publishedRoute;
  }

  public Boolean getIsEditingLocked() {
    return isEditingLocked;
  }

  public void setIsEditingLocked(Boolean isEditingLocked) {
    this.isEditingLocked = isEditingLocked;
  }

}
