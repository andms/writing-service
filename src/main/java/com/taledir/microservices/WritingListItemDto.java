package com.taledir.microservices;

import java.util.Date;

public class WritingListItemDto {

	private Long id;

  private String title;
  
  private String publishedRoute;

  private String snippet;

	private Date lastPublishedDate;

	private Date lastModifiedDate;

  public WritingListItemDto(Long id, String title, String publishedRoute,
    String snippet, Date lastPublishedDate, Date lastModifiedDate) {
    this.id = id;
    this.title = title;
    this.publishedRoute = publishedRoute;
    this.snippet = snippet;
    this.lastPublishedDate = lastPublishedDate;
    this.lastModifiedDate = lastModifiedDate;
  }
  
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

  public String getpublishedRoute() {
    return publishedRoute;
  }

  public void setpublishedRoute(String publishedRoute) {
    this.publishedRoute = publishedRoute;
  }

  public String getSnippet() {
    return snippet;
  }

  public void setSnippet(String snippet) {
    this.snippet = snippet;
  }

	public Date getLastPublishedDate() {
		return lastPublishedDate;
	}

	public void setLastPublishedDate(Date lastPublishedDate) {
		this.lastPublishedDate = lastPublishedDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}