package com.taledir.microservices;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "writing")
public class Writing {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="owner_id")
  private Author owner;

  @Column(name = "published_id")
  @JsonIgnore
	private Long publishedId;

	@Column(name = "title")
	private String title;

	@Column(name = "last_published_title")
	private String lastPublishedTitle;

	@Column(name = "subtitle")
  private String subtitle;
  
	@Column(name = "published_route", unique = true)
	private String publishedRoute;

	@Column(name = "snippet")
	private String snippet;

	@PastOrPresent
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_published_date")
	private Date lastPublishedDate;

	@PastOrPresent
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;

	@Column(name = "is_editing_locked", nullable = false)
  private Boolean isEditingLocked = false;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "writing_author", 
		joinColumns = @JoinColumn(name = "writing_id"), 
		inverseJoinColumns = @JoinColumn(name = "author_id"))
	Set<Author> authors;

	@OneToOne(mappedBy = "writing", cascade = CascadeType.ALL, optional = false, fetch = FetchType.LAZY)
	private WritingBody writingBody;
	
	protected Writing() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPublishedId() {
		return publishedId;
	}

	public void setPublishedId(Long publishedId) {
		this.publishedId = publishedId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

  public String getLastPublishedTitle() {
    return lastPublishedTitle;
  }

  public void setLastPublishedTitle(String lastPublishedTitle) {
    this.lastPublishedTitle = lastPublishedTitle;
  }

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

  public String getSnippet() {
    return snippet;
  }

  public void setSnippet(String snippet) {
    this.snippet = snippet;
  }

	public Date getLastPublishedDate() {
		return lastPublishedDate;
	}

	public void setLastPublishedDate(Date lastPublishedDate) {
		this.lastPublishedDate = lastPublishedDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public WritingBody getWritingBody() {
		return writingBody;
	}

  public void setWritingBody(WritingBody writingBody) {
      if (writingBody == null) {
          if (this.writingBody != null) {
              this.writingBody.setWriting(null);;
          }
      }
      else {
        writingBody.setWriting(this);
      }
      this.writingBody = writingBody;
  }

  public Author getOwner() {
    return owner;
  }

  public void setOwner(Author owner) {
    this.owner = owner;
  }

  public String getPublishedRoute() {
    return publishedRoute;
  }

  public void setPublishedRoute(String publishedRoute) {
    this.publishedRoute = publishedRoute;
  }

  public Boolean getIsEditingLocked() {
    return isEditingLocked;
  }

  public void setIsEditingLocked(Boolean isEditingLocked) {
    this.isEditingLocked = isEditingLocked;
  }
  
}
