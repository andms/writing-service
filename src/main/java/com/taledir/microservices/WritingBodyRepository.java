package com.taledir.microservices;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WritingBodyRepository extends JpaRepository<WritingBody, Long>{

}