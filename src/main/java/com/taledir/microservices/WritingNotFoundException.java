package com.taledir.microservices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class WritingNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public WritingNotFoundException(String arg0) {
        super(arg0);
    }
    
}