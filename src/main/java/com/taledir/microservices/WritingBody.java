package com.taledir.microservices;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "writing_body")
public class WritingBody {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "body")
	private String body;

	@OneToOne
	@MapsId
	@JoinColumn(name = "id")
	private Writing writing;
	
	protected WritingBody() {

	}

	public WritingBody(String body) {
		super();
		this.body = body;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Writing getWriting() {
		return writing;
	}

	public void setWriting(Writing writing) {
		this.writing = writing;
	}

}