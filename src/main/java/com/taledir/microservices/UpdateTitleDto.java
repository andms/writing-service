package com.taledir.microservices;

import javax.validation.constraints.NotBlank;

import javax.validation.constraints.Pattern;

public class UpdateTitleDto {

	@Pattern(regexp = "^.*[a-zA-Z0-9]+.*$",
		message = "Title must contain at least one letter or one number.")
  @NotBlank(message = "Title must be specified")
	private String title;
  

  protected UpdateTitleDto() {
  }

	public UpdateTitleDto(String title) {
		this.title = title;
	}

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

}
