package com.taledir.microservices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WritingController {

	Logger logger = LoggerFactory.getLogger(WritingController.class);

  @Autowired
  WritingRepository writingRepository;

	@Autowired
	AuthorRepository authorRepository;

	@Autowired
	WritingService writingService;

	@GetMapping("/writing/drafts")
	public Set<WritingListItemDto> retrieveDrafts(
    @CookieValue(value = "token", defaultValue = "") String token) {

    if(token == null || token.length() == 0) {
      throw new WritingBadRequestException("Cannot access writings, please try to login first");
    }

		String username = writingService.getTokenUsername(token);

		if(username == null || username.length() == 0) {
			throw new WritingBadRequestException("Cannot access writings, please try to login first");
		}

		Set<WritingListItemDto> writings = writingRepository.findDraftsByUsername(username);
		
		return writings;
	}

	@GetMapping("/writing/published")
	public Set<WritingListItemDto> retrievePublished(
    @CookieValue(value = "token", defaultValue = "") String token) {

    if(token == null || token.length() == 0) {
      throw new WritingBadRequestException("Cannot access writings, please try to login first");
    }

		String username = writingService.getTokenUsername(token);

		if(username == null || username.length() == 0) {
			throw new WritingBadRequestException("Cannot access writings, please try to login first");
		}

		Set<WritingListItemDto> writings = writingRepository.findPublishedByUsername(username);
		
		return writings;
	}

	@GetMapping("/writing/{id}")
	public WritingDto retrieveWriting(
		@CookieValue(value = "token", defaultValue = "") String token,
		@PathVariable Long id) {

		if(token == null || token.length() == 0 ||
			!writingService.isValidReadWritePermissions(id, token)) {
			throw new WritingBadRequestException(
				"Cannot access story, please try to login first, and verify your permissions.");
		}

		WritingDto writingDto = writingService.retrieveWriting(id);
		return writingDto;
	}

	@PostMapping("/writing/create")
	public ResponseEntity<Object> createStory(
    @CookieValue(value = "token", defaultValue = "") String token) {

		String username = writingService.getTokenUsername(token);
		
		if(username == null || username.length() == 0) {
			throw new WritingBadRequestException(
				"Cannot create new story.  Please try to login first.");
		}

		Author author = authorRepository.findOneByUsername(username);

		if(author == null) {
			logger.error("Logged in user attempted to create story, but their username does not exist in writing aggregate!");
			throw new WritingBadRequestException(
				"Cannot create new story.  User Profile does not exist.");
    }
    
    Long id = writingRepository.findOneUnmodifiedByUsername(username);

    //first attempt to use an existing unmodified writing, then create new one
    if(id == null) {

      Writing writing = new Writing();

      Set<Author> authors = new HashSet<Author>();
      authors.add(author);
      writing.setAuthors(authors);

      //initial creator of writing is always first owner
      writing.setOwner(author);

      List<Object> ops = new ArrayList<Object>();
      Map<String, String> initOp = Collections.singletonMap("insert", "\n");
      ops.add(initOp);
      Delta delta = new Delta(ops);
      ObjectMapper objectMapper = new ObjectMapper();
      String body;
      try {
        body = objectMapper.writeValueAsString(delta);
      } catch (JsonProcessingException e) {
        throw new WritingBadRequestException(e.getMessage());
      }

      WritingBody writingBody = new WritingBody();
      writingBody.setBody(body);

      writing.setWritingBody(writingBody);

      Writing savedWriting = writingRepository.save(writing);

      id = savedWriting.getId();
    }

		Map<String, Long> response = new HashMap<String, Long>();
    response.put("id", id);

		return ResponseEntity.ok().body(response);

	}

	@PostMapping("/writing/save/title/{id}")
	public ResponseEntity<Object> updateWritingTitle(
		@CookieValue(value = "token", defaultValue = "") String token, @PathVariable Long id, 
		@Valid @RequestBody UpdateTitleDto updateTitleDto, BindingResult bindingResult) {

    if(bindingResult.hasErrors()) {
      if(bindingResult.getFieldError() != null) {
        throw new WritingBadRequestException(bindingResult.getFieldError().getDefaultMessage());
      }
    }
  
		if(token == null || token.length() == 0 ||
		!writingService.isValidReadWritePermissions(id, token)) {
			throw new WritingBadRequestException(
				"Cannot update story title.  " + 
				"Please try to login first, and verify your permissions.");
		}

		Writing writing = writingRepository.getOne(id);

		if(writing == null) {
			throw new WritingNotFoundException("Cannot update title, story does not exist");
    }
    
    if(writing.getIsEditingLocked()) {
      throw new WritingBadRequestException("Cannot update story title, editing is currently locked.");
    }

    writing.setTitle(updateTitleDto.getTitle());
    writing.setLastModifiedDate(new Date());
  	writingRepository.save(writing);
    
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return ResponseEntity.ok().body(body);
  }

	@PostMapping("/writing/save/body/{id}")
	public ResponseEntity<Object> updateWritingBody(
		@CookieValue(value = "token", defaultValue = "") String token,
		@PathVariable Long id, @RequestBody Delta newDelta) {
		
		if(token == null || token.length() == 0 ||
		!writingService.isValidReadWritePermissions(id, token)) {
			throw new WritingBadRequestException(
				"Cannot save changes for story.  " + 
				"Please try to login first, and verify your permissions.");
		}

    writingService.updateStoryBody(id, newDelta);
    
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return ResponseEntity.ok().body(body);
  }

	@PostMapping("/writing/publish/{id}")
	public ResponseEntity<Published> publish(
		@CookieValue(value = "token", defaultValue = "") String token,
		@PathVariable Long id) {

    Writing writing = writingService.getWritingForOwner(token, id);

    boolean isLockedOnlyForPublishing = true;

    if(!writing.getIsEditingLocked()) {
      writingService.lockEditing(writing);
    }
    else {
      isLockedOnlyForPublishing = false;
    }

    Published published;

    try {
      published = writingService.publish(token, writing);
    }
    finally {
      //regardless of failure, need to unlock editing
      //however, only unlock if lock was made specifically for publishing
      if(isLockedOnlyForPublishing) {
        writingService.unlockEditing(writing);
      }  
    }

    return ResponseEntity.ok().body(published);    

  }

	@PostMapping("/writing/lock/{id}")
	public ResponseEntity<Object> editLock(
		@CookieValue(value = "token", defaultValue = "") String token,
		@PathVariable Long id) {

    Writing writing = writingService.getWritingForOwner(token, id);
    
    writingService.lockEditing(writing);

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return ResponseEntity.ok().body(body);
  }

	@PostMapping("/writing/unlock/{id}")
	public ResponseEntity<Object> editUnlock(
		@CookieValue(value = "token", defaultValue = "") String token,
		@PathVariable Long id) {

    Writing writing = writingService.getWritingForOwner(token, id);

    writingService.unlockEditing(writing);

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return ResponseEntity.ok().body(body);
  }

	@GetMapping("/writing/lockStatus/{id}")
	public ResponseEntity<Object> retrieveEditLockStatus(
		@CookieValue(value = "token", defaultValue = "") String token,
		@PathVariable Long id) {

		if(token == null || token.length() == 0 ||
			!writingService.isValidReadWritePermissions(id, token)) {
			throw new WritingBadRequestException(
				"Cannot access story, please try to login first, and verify your permissions.");
		}

    Boolean isEditingLocked = writingRepository.findIsEditingLockedById(id);

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("isEditingLocked", isEditingLocked);

		return ResponseEntity.ok().body(body);
	}


}