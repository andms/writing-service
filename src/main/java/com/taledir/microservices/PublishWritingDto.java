package com.taledir.microservices;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class PublishWritingDto {

  @NotEmpty
	private String token;

  private Long id;

	@Pattern(regexp = "^.*[a-zA-Z0-9]+.*$",
		message = "Title must contain at least one letter or one number.")
  @NotEmpty(message = "Title must be specified")
	private String title;

  @NotEmpty
  private String body;
  
  private String subtitle;
  
  @NotEmpty
  private String slug;

  @NotEmpty
  private String snippet;

  @NotEmpty
  private Set<String> authorUsernames;

  public PublishWritingDto(String token, Long id, String title, String body, 
    String subtitle, String slug, String snippet, Set<String> authorUsernames) {
    this.token = token;
    this.id = id;
    this.title = title;
    this.body = body;
    this.subtitle = subtitle;
    this.slug = slug;
    this.snippet = snippet;
    this.authorUsernames = authorUsernames;
	}

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getSnippet() {
    return snippet;
  }

  public void setSnippet(String snippet) {
    this.snippet = snippet;
  }

  public Set<String> getAuthorUsernames() {
    return authorUsernames;
  }

  public void setAuthorUsernames(Set<String> authorUsernames) {
    this.authorUsernames = authorUsernames;
  }
	
}